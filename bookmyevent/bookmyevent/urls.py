from django.contrib import admin
from django.conf.urls import patterns, include, url
from frontend.views import *


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', IndexView.as_view(), name='home'),
    url(r'^frontend/', include('frontend.urls')),
    url(r'^logout/$', LogOutView.as_view(), name='logout'),
 	url(r'^changepassword/$', ChangePasswordFormView.as_view(), name='changepassword'),
    url(r'^account_activation/(?P<pk>[\d]+)/$', AccountActivationView.as_view(), name='account_activation'),
    url(r'^forgot_password_request/(?P<slug>[-\w\d]+)/$', ForgotPasswordLinkView.as_view(), name='forgot_password_request'),
]
