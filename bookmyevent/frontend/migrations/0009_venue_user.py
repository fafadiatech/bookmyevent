# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0008_auto_20160411_0700'),
    ]

    operations = [
        migrations.AddField(
            model_name='venue',
            name='user',
            field=models.ForeignKey(blank=True, to='frontend.Role', null=True),
        ),
    ]
