# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('firstname', models.CharField(max_length=255)),
                ('lastname', models.CharField(max_length=255)),
                ('useremail', models.CharField(max_length=255)),
                ('contactno', models.CharField(max_length=10)),
                ('password', models.CharField(max_length=255)),
            ],
        ),
        migrations.AddField(
            model_name='bookevent',
            name='booking_email',
            field=models.EmailField(max_length=254, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='bookevent',
            name='booking_name',
            field=models.CharField(default='abc', max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='bookevent',
            name='booking_no',
            field=models.CharField(default='1', max_length=10),
            preserve_default=False,
        ),
    ]
