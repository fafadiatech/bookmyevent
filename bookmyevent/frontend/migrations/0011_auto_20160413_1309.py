# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0010_auto_20160413_1258'),
    ]

    operations = [
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=20, null=True, blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='venue',
            name='address_location',
            field=models.ForeignKey(blank=True, to='frontend.Location', null=True),
        ),
    ]
