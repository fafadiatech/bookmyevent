# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0002_auto_20160409_0651'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='contactno',
            field=models.CharField(default='123', max_length=10),
            preserve_default=False,
        ),
    ]
