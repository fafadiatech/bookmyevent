# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0011_auto_20160413_1309'),
        ('frontend', '0010_forgotpassword'),
    ]

    operations = [
    ]
