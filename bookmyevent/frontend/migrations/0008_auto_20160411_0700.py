# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0007_merge'),
    ]

    operations = [
        migrations.RenameField(
            model_name='role',
            old_name='vendor_user',
            new_name='name',
        ),
    ]
