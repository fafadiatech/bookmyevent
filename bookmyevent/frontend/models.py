from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import AbstractUser, Permission

# Create your models here.

MENU_CHOICES = (('Veg', 'Veg'), ('Non-Veg', 'Non-Veg'))

# Create your models here.

class Role(models.Model):
	name = models.CharField(max_length=20, blank=True, null=True)

	def __unicode__(self):
		return self.name


class Location(models.Model):
	name = models.CharField(max_length=20, blank=True, null=True)

	def __unicode__(self):
		return self.name


class Profile(AbstractUser):
	contactno=models.CharField(max_length=10, null=False)
	user_type = models.ForeignKey(Role, blank=True, null=True)
	profile_pic = models.ImageField(upload_to="Profile_images",blank=True,null=True)

	def __unicode__(self):
		return self.first_name


class Venue(models.Model):
	user = models.ForeignKey(Profile, blank=True, null=True)
	name = models.CharField(max_length=255, blank=True, null=True)
	address1 = models.CharField(max_length=255, blank=True, null=True)
	address2 = models.CharField(max_length=255, blank=True, null=True)
	address3 = models.CharField(max_length=255, blank=True, null=True)
	address4 = models.CharField(max_length=255, blank=True, null=True)
	address_location = models.ForeignKey(Location, blank=True, null=True)
	pincode = models.CharField(max_length=6, blank=True, null=True)
	contact_no = models.CharField(max_length=10, blank=True, null=True)
	capacity = models.CharField(max_length=255, blank=True, null=True)
	menu = models.CharField(max_length=30, blank=True, choices=MENU_CHOICES)
	price = models.CharField(max_length=255, blank=True, null=True)
	venu_pic_1 = models.ImageField(upload_to="Venue_images", blank=True)
	venu_pic_2 = models.ImageField(upload_to="Venue_images", blank=True)
	venu_pic_3 = models.ImageField(upload_to="Venue_images", blank=True)

	def __unicode__(self):
		return self.name


class BookEvent(models.Model):
	user = models.ForeignKey(Profile, blank=True, null=True)
	venue = models.ForeignKey(Venue, blank=True, null=True)
	date = models.DateField()
	totalguest = models.CharField(max_length=255)
	menu = models.CharField(max_length=30, choices=MENU_CHOICES)
	booking_name = models.CharField(max_length=255)
	booking_no = models.CharField(max_length=10)
	booking_email = models.EmailField(blank=True, null=True)

	def __unicode__(self):
		return self.booking_name
		

class ForgotPassword(models.Model):

    """
    Model  used to store information of Forgot password requests
    """

    user = models.ForeignKey(Profile)
    hashkey = models.CharField(max_length = 155)
    timestamp = models.DateField()

    def __unicode__(self):
        return self.hashkey
