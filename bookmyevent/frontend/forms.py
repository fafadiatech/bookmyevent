import re
from django import forms
from frontend.models import *
from django.contrib import auth

class SignupForm(forms.ModelForm):
	
	"""
	Signup form
	"""
	class Meta:
		model = Profile
		fields = ('username','first_name', 'last_name', 'email', 'password', 'contactno', 'user_type','profile_pic')

	def save(self, commit=True):
		instance = super(SignupForm, self).save()
		return instance
		
	def clean(self):
		cleaned_data = super(SignupForm, self).clean()

		try:
			username = cleaned_data.get('username')
		except:
			username = ""

		if not username:
			msg = "This Field Is Required"
			self._errors["username"] = self.error_class([msg])

		try:
			first_name = cleaned_data.get('first_name')
		except:
			first_name = ""

		if not first_name:
			msg = "This Field Is Required"
			self._errors["first_name"] = self.error_class([msg])

		try:
			last_name = cleaned_data.get('last_name')
		except:
			last_name = ""

		if not last_name:
			msg = "This Field Is Required"
			self._errors["last_name"] = self.error_class([msg])


		try:
			email = cleaned_data.get('email')
		except:
			email = ""

		if not email:
			msg = "This Field Is Required"
			self._errors["email"] = self.error_class([msg])

		try:
			password = cleaned_data.get("password")
		except:
			password = ""

		if not password:
			msg = "This Field is Required"
			self._errors["password"] = self.error_class([msg])
		try:
			contactno = cleaned_data.get("contactno")
		except:
			contactno = ""

		#contact number validation
		if contactno:
			if not ''.join(re.sub('[+-]', '', contactno).split()).isdigit():
				msg = "Valid Contact Number Is Required"
				self._errors["contactno"] = self.error_class([msg])

		# user = Profile.objects.filter(email=email).exists()
		# if not user == False:
		# 	msg = "A User With Same Email Id Already Registered."
		# 	self._errors["email"] = self.error_class([msg])

		return self.cleaned_data




	
class BookingForm(forms.ModelForm):

	"""
	Booking Form
	"""

	class Meta:
		model = BookEvent
		fields = ('date', 'totalguest','menu','booking_name','booking_email','booking_no')



# UpdateBookingList Form

# class UpdateBookingListForm(forms.Form):
# 	"""docstring for UpdateBookingListForm"""

# 	class Meta:
# 		model = BookEvent
# 		fields = ('date', 'no_of_people','menu','name','email_id','contact_no')

# 	def save(self, commit=True):
# 		instance = super(UpdateBookingListForm, self).save()
# 		return instance

# 	def clean(self):
# 		cleaned_data = super(UpdateBookingListForm, self).clean()

# 		try:
# 			date = cleaned_data.get('date')
# 		except:
# 			date = ""

# 		if not date:
# 			msg = "This Field Is Required"
# 			self._errors["date"] = self.error_class([msg])

# 		try:
# 			no_of_people = cleaned_data.get('no_of_people')
# 		except:
# 			no_of_people = ""

# 		if not no_of_people:
# 			msg = "This Field Is Required"
# 			self._errors["no_of_people"] = self.error_class([msg])

# 		try:
# 			menu = cleaned_data.get('menu')
# 		except:
# 			menu = ""

# 		if not menu:
# 			msg = "This Field Is Required"
# 			self._errors["menu"] = self.error_class([msg])

# 		try:
# 			name = cleaned_data.get('name')
# 		except:
# 			name = ""

# 		if not name:
# 			msg = "This Field Is Required"
# 			self._errors["name"] = self.error_class([msg])

# 		try:
# 			email_id = cleaned_data.get('email_id')
# 		except:
# 			email_id = ""

# 		if not email_id:
# 			msg = "This Field Is Required"
# 			self._errors["email_id"] = self.error_class([msg])

# 		try:
# 			contact_no = cleaned_data.get('contact_no')
# 		except:
# 			contact_no = ""

# 		if not contact_no:
# 			msg = "This Field Is Required"
# 			self._errors["contact_no"] = self.error_class([msg])

# 		return self.cleaned_data



class LoginForm(forms.Form):
	login_username = forms.CharField()
	login_password = forms.CharField(widget=forms.PasswordInput)

	def clean(self):
		cleaned_data = super(LoginForm, self).clean()

		username = cleaned_data.get('login_username')
		password = cleaned_data.get('login_password')

		if not (username or password):
		    msg = "User name/Email and Password Is Required"
		    self._errors["password"] = self.error_class([msg])
		    return self.cleaned_data

		user = auth.authenticate(username=username, password=password)
		if user is None:
		    msg = "Username/Email or Password Is Incorrect"
		    self._errors["password"] = self.error_class([msg])
		    return self.cleaned_data

		if not user.is_active:
		    msg = "Your Account is not activated"
		    self._errors["password"] = self.error_class([msg])
		    return self.cleaned_data
		return self.cleaned_data


class UpdateBookingForm(forms.ModelForm):

	"""
	UpdateBooking Form
	"""

	class Meta:
		model = BookEvent
		exclude = []


class ForgotPasswordForm(forms.Form):
	
	email = forms.CharField()

	def clean(self):
		cleaned_data = super(ForgotPasswordForm, self).clean()
		# import pdb
		# pdb.set_trace()
		try:
			email = cleaned_data.get("email")
		except:
			email = ""

		if not email:
			msg = "This Field  Is Required"
			self._errors["email"] = self.error_class([msg])
			return self.cleaned_data
		
		try:
			user = Profile.objects.get(email=email)
		except:
			user = ""

		if not user:
			msg = "Invalid Email Address, No Such Email Found"
			self._errors["email"] = self.error_class([msg])
			return self.cleaned_data


class ChangePasswordForm(forms.Form):
	"""
	Change Password Form
	"""
	user_ = forms.CharField(label="user", max_length=50, required=False)
	current_password = forms.CharField(widget=forms.PasswordInput(), label="current_password", max_length=50)
	new_password = forms.CharField(widget=forms.PasswordInput(), label="new_password", max_length=50)
	confirm_password = forms.CharField(widget=forms.PasswordInput(), label="confirm_password", max_length=50)

	def clean(self):
		cleaned_data = super(ChangePasswordForm, self).clean()
		user_id = cleaned_data.get('user_')
		
		user1 = Profile.objects.get(user_type=user_id);

		try:
			password = cleaned_data.get('current_password')
		except:
			password = ''

		if not password:
			msg = "This Field Required"
			self._errors["password"] = self.error_class([msg])

		current_password = cleaned_data['current_password'];
		
		if not user1.check_password(current_password):
			msg = "Old Password Does Not Match With User"
			self._errors["current_password"] = self.error_class([msg])

		try:
			newpassword = cleaned_data.get('new_password')
		except:
			newpassword = ''

		if not newpassword:
			msg = "This Field Required"
			self._errors["new_password"] = self.error_class([msg])

		try:
			confirmpassword = cleaned_data.get('confirm_password')
		except:
			confirmpassword = ''

		if not confirmpassword:
			msg = "Confirm Password Is Required"
			self._errors["confirm_password"] = self.error_class([msg])

		if newpassword == confirmpassword:
			pass
		else:
			msg="Both Passwords do not match"
			self._errors["confirm_password"] = self.error_class([msg])

		return self.cleaned_data


class ChangePasswordRequestForm(forms.Form):

	"""
	Change Password Form
	"""

	password = forms.CharField(widget=forms.PasswordInput)
	confirm_password = forms.CharField(widget=forms.PasswordInput)

	def clean(self):
		cleaned_data = super(ChangePasswordRequestForm, self).clean()

		try:
			password = cleaned_data.get('password')
		except:
			password = ''

		if not password:
			msg = "This Field Required"
			self._errors["password"] = self.error_class([msg])

		try:
			confirm_password = cleaned_data.get('confirm_password')
		except:
			confirm_password = ''

		if not confirm_password:
			msg = "Confirm Password Is Required"
			self._errors["confirm_password"] = self.error_class([msg])

		if password == confirm_password:
			pass
		else:
			msg="Both Passwords do not match"
			self._errors["confirm_password"] = self.error_class([msg])

		return self.cleaned_data
