from django.contrib import admin
from models import Venue, BookEvent, Role, Profile,ForgotPassword,Location

# Register your models here.

admin.site.register(Role)
admin.site.register(Profile)
admin.site.register(Venue)
admin.site.register(BookEvent)
admin.site.register(ForgotPassword)
admin.site.register(Location)