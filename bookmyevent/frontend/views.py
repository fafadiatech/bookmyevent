import json
import random
from django.conf import settings
from django.shortcuts import render
from datetime import datetime
import hashlib
import uuid
from uuid import uuid4
from django.views.generic import TemplateView, ListView, DetailView, FormView, UpdateView, RedirectView, View
from django.http import HttpResponse
from django.contrib.auth.models import AbstractUser
from django.core.urlresolvers import reverse, reverse_lazy
from datetime import datetime
from datetime import timedelta
from frontend.forms import *
from frontend.models import *
from django.contrib.auth import login,logout,authenticate
from django.contrib import messages
from django.core.mail import send_mail
from django.conf import settings
from django.core.paginator import Paginator
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, get_object_or_404


def render_to_json_response(context, **response_kwargs):
	data = json.dumps(context)
	response_kwargs['content_type'] = 'application/json'
	return HttpResponse(data, **response_kwargs)



class IndexView(TemplateView):
	"""
	docstring for IndexView
	"""
	template_name = 'frontend/index.html'

	def get_context_data(self, **kwargs):
		context = super(IndexView, self).get_context_data(**kwargs)
		context['roles'] = Role.objects.all()
		return context


class SignupView(FormView):

	"""
	Signup Page
	"""

	template_name = 'frontend/index.html'
	form_class = SignupForm
	success_url = '/'

	def get_context_data(self, **kwargs):
		context = super(SignupView, self).get_context_data(**kwargs)
		context['roles'] = Role.objects.all()
		return context

	def send_mail_to_user(self,*args):
		email,activation_link=args
		send_mail('Succeful signup', activation_link, 'yamini@fafadiatech.com',[email], fail_silently=False);


	def form_valid(self, form):
		username=form.cleaned_data['username']
		form.save()
		email = form.cleaned_data['email']
		password = form.cleaned_data['password']
		user = Profile.objects.get(username=username)
		user.set_password(password)
		user.save()
		activation_link = '{0}/account_activation/{1}'.format(settings.SITE_NAME, user.id)
		self.send_mail_to_user(email,activation_link)
		msg = "You've successfully signed up, Check your email for Login details."
		return self.render_to_json_response({"msg":msg})

	def form_invalid(self, form):
		print form.errors
		return self.render_to_json_response(form.errors, status=400)

	def post(self, request, *args, **kwargs):
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		if form.is_valid()																																																																																:
			return self.form_valid(form)
		else:
			return self.form_invalid(form)


class BookingView(ListView):
	template_name = 'frontend/mybookings.html'
	model = BookEvent
	paginate_by = 10

	def get_context_data(self, **kwargs):
		context = super(BookingView, self).get_context_data(**kwargs)
		context['booking_objects'] = BookEvent.objects.filter(user=self.request.user.id);
		return context


class VenueView(DetailView):
	template_name = 'frontend/venue-details.html'
	model = Venue


class BookingUpdateView(UpdateView):
	model = BookEvent
	template_name = 'frontend/index.html'
	context_object_name = 'object'
	success_url = reverse_lazy('booking-list')
	form_class = UpdateBookingForm

	def get_context_data(self, **kwargs):
		context = super(BookingUpdateView, self).get_context_data(**kwargs)
		context['booking_name'] = BookEvent.objects.all().order_by('booking_name')
		return context
		

class BookingFormView(FormView):

	"""
	Venue Booking Form
	"""

	template_name = "/frontend/venue-details.html"
	form_class = BookingForm

	def form_valid(self, form):
		form.save()		
		email = form.cleaned_data['booking_email']	
		self.send_confirmationmail_to_user(email)
		send_mail('Booking Succesful','Hello user,you succesfully book this hall',[email,'yamini@fafadiatech.com'], fail_silently=False);
		msg = "Thank you for Booking. We will revert back soon."

		return self.render_to_json_response({"msg":msg})
		
	def form_invalid(self, form):
		return self.render_to_json_response(form.errors, status=400)


class LoginAjaxView(FormView):
	template_name = '/frontend/index.html'
	form_class = LoginForm
	success_url = "/"
		
	def form_valid(self, form):
		username = form.cleaned_data['login_username']
		password = form.cleaned_data['login_password']

		user = authenticate(username=username, password=password) 
		if user is not None:
			if user.is_active:
				login(self.request, user)
				if user.user_type == 'vendor':
					return render_to_json_response({"url":self.get_success_url()})
				else:
					return render_to_json_response({"url":self.get_success_url()})
			else:
				return self.form_invalid(form)
		else:
			return self.form_invalid(form)

	def form_invalid(self, form):
		return render_to_json_response(form.errors, status=400)

	def post(self, request, *args, **kwargs):
		print self.request.POST
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		if form.is_valid():
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def get_success_url(self, **kwargs):
		next = self.request.GET.get('next')
		if next:
			redirect_to = next
		else:
			redirect_to = self.success_url

		return redirect_to

 
class LogOutView(RedirectView):
	"""
	Signout view
	"""
	def get_redirect_url(self):
		logout(self.request)
		return reverse('home')


class Searchresultpage(TemplateView):
	template_name= 'frontend/searchresultpage.html'


class ChangePasswordFormView(FormView):
	"""
	Changing Password Form
	"""

	template_name = "frontend/change_password.html"
	form_class = ChangePasswordForm
	success_url = '/'
	# changepassword self.success_url
	def form_valid(self, form):
		password = form.cleaned_data['new_password']
		current_user = self.request.user
		current_user.set_password(password)
		current_user.save()
		current_user.backend = settings.AUTHENTICATION_BACKENDS[0]
		
		msg = "Your password is successfully reset."
		messages.success(self.request, msg)
		return HttpResponseRedirect('/changepassword/')

	def form_invalid(self, form):
		return render_to_json_response(form.errors, status=400)

	def post(self, request, *args, **kwargs):
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		if form.is_valid():
			return self.form_valid(form)
		else:
			return self.form_invalid(form)
			

class AccountActivationView(TemplateView):
	template_name= 'frontend/account_activation.html'
	form_class = SignupForm

	def get(self, request, *args, **kwargs):
		context = self.get_context_data(**kwargs)
		userpk = Profile.objects.filter(pk= self.kwargs['pk']).update(is_active=True)
		return self.render_to_response(context)

class SearchVenueLocationTypeahead(View):
	def get(self, request, *args, **kwargs):
		station_dict = {}
		result={}
		stations = Location.objects.all()
		for station in stations:
			station_dict[station.id] = [station.name]
		return HttpResponse(json.dumps(station_dict), content_type="application/json")


class SearchResultForVenue(ListView):
	template_name = 'frontend/search-result.html'
	model = Venue
	paginate_by = 5
	context_object_name = 'venue_list'

	def get_context_data(self, **kwargs):
		context = super(SearchResultForVenue, self).get_context_data(**kwargs)
		context['Venus'] = Venue.objects.filter(user=self.request.user.id);
		return context

	def get_queryset(self):
		location_id = self.kwargs['location_id']
		venue_list = Venue.objects.filter(address_location__id=location_id)
		return venue_list


class ForgotPasswordView(FormView):

	"""
	Forget Password Request Form (entering email id)
	"""

	template_name = "frontend/index.html"
	form_class = ForgotPasswordForm

	def form_valid(self, form):
		email = form.cleaned_data['email']
		user = Profile.objects.get(email=email)
		ForgotPassword.objects.filter(user=user).delete()	
		u = uuid.uuid4()
		salt = u.hex
		h = hashlib.sha256()
		h.update(user.email+salt)
		hashed_key = h.hexdigest()
		activation_link = '{0}/forgot_password_request/{1}'.format(settings.SITE_NAME,hashed_key)
		ForgotPassword.objects.create(user = user, hashkey = hashed_key, timestamp = datetime.now())
		send_mail('Forgot password request',activation_link, 'yamini@fafadiatech.com',[email], fail_silently=False);
		msg = "Check your email for further assitance for changing password"
		return self.render_to_json_response({"msg":msg}, status=200)

	def form_invalid(self, form):
		return self.render_to_json_response(form.errors, status=400)

	def post(self, request, *args, **kwargs):
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		if form.is_valid()																																																																																:
			return self.form_valid(form)
		else:
			return self.form_invalid(form)


class ForgotPasswordLinkView(FormView):

	template_name = "frontend/forgot_password_request.html"
	form_class = ChangePasswordRequestForm
	success_url = '/'

	def get_context_data(self, **kwargs):
		context = super(ForgotPasswordLinkView, self).get_context_data(**kwargs)
		context['obj'] =  ForgotPassword.objects.get(hashkey=self.kwargs['slug'])
		return context

	def form_valid(self, form):
		obj = ForgotPassword.objects.get(hashkey=self.kwargs['slug'])		
		password = form.cleaned_data['password']
		user = Profile.objects.get(pk = obj.user_id)
		user.set_password(password)
		user.save()
		ForgotPassword.objects.filter(hashkey = self.kwargs['slug']).delete()
		msg = "Your password is successfully reset."
		messages.success(self.request, msg)
		return redirect(self.success_url)

	def post(self, request, *args, **kwargs):
		obj = get_object_or_404(ForgotPassword, hashkey=self.kwargs['slug'])
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		if form.is_valid():
			return self.form_valid(form)
		else:
			return self.form_invalid(form)
