from frontend.models import Profile
from django.contrib.auth.models import check_password

class UsernameAuthBackend(object):
	"""
    A custom authentication backend. Allows users to log in using their email and mobile number
	"""
	def authenticate(self, username=None, password=None):
		"""
		Authentication method
		"""
		if '@' in username:
			try:
				user = Profile.objects.get(email__iexact=username)
				if user.check_password(password):
					return user
			except Profile.DoesNotExist:
				return None
		else:
			try:
				user = Profile.objects.get(username__iexact=username)
				if user.check_password(password):
					return user
			except Profile.DoesNotExist:
				return None

	def get_user(self,user_id):
		try:
			user = Profile.objects.get(pk=user_id)
			if user.is_active:
				return user
			return None
		except Profile.DoesNotExist:
			return None