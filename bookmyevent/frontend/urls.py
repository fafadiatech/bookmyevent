from django.contrib import admin
from django.conf.urls import patterns, include, url
from django.views.generic.base import TemplateView
from django.views.generic.list import ListView
from frontend.views import *
from frontend import views

urlpatterns = patterns('',
  url(
		regex=r'^signup$',
		view=SignupView.as_view(),
		name='signup'
    ),

	url(
		regex=r'^booking-list/$',
		view=BookingView.as_view(),
		name='booking-list'
    ),
    	
	url(
		regex=r'^venue_details/(?P<pk>[\d]+)/$',
		view=VenueView.as_view(),
		name='venue_details'
    ),

	url(
		regex=r'^venue-booking/$',
		view=BookingFormView.as_view(),
		name='venue-booking'
    ),

	url(
		regex=r'^login',
		view=LoginAjaxView.as_view(),
		name='login'
    ),

    url(
		regex=r'^searchresultpage/$',
		view=Searchresultpage.as_view(),
		name='searchresultpage'
    ),

    url(
		regex=r'^booking-event-update/(?P<pk>[\d]+)/$',
		view=BookingUpdateView.as_view(),
		name='booking-event-update'
    ),

    url(
		regex=r'^forgot_password$',
		view=ForgotPasswordView.as_view(),
		name='forgot_password'
    ),

    url(
		regex=r'^search-venue-location-typeahead/$',
		view=SearchVenueLocationTypeahead.as_view(),
		name='search-venue-location-typeahead'
    ),

    url(
		regex=r'^search-results-venue/(?P<location_id>[-\w\d\+]+)/$',
		view=SearchResultForVenue.as_view(),
      	name='search-results-venue'
	),

    url(
		regex=r'^password_setting_for_forgotpassword/(?P<slug>[-\w\d]+)/$',
		view=ForgotPasswordLinkView.as_view(),
		name='password_setting_for_forgotpassword'
    ),
)