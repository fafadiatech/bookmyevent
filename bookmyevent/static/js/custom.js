// to display date
$("#datepicker").datepicker({ 
	autoclose: true, 
	todayHighlight: true
	}).datepicker('update', new Date());

$("#datepicker1").datepicker({
	format: 'mm/dd/yyyy',
});
// jQuery methods go here...
$('#forgotpassword').on('click', function () {
    $('#myModal').modal('hide');
    $('#forgotpasswd').modal('show');
	});

$('#signup').on('click', function () {
    $('#myModal').modal('hide');
    $('#signupModal').modal('show');
    });

$('#log-in').on('click', function () {
    $('#signupModal').modal('hide');
    $('#myModal').modal('show');
    });

// signup proceesing start
$("#signupbtn").click(function() {

	csrfval = $('input[name="csrfmiddlewaretoken"]').val();
	var username = $("#username").val();
	var first_name = $("#first_name").val();
	var last_name = $("#last_name").val();
	var email = $("#email").val();
	var contactno = $("#contactno").val();
	var password = $("#password").val();
	var user_type = $("#user_type").val();
	var profile_pic = $("#profile_pic").val();

	$.ajax({
	url: "/frontend/signup",
	type: "POST",
	data: {"username":username, "first_name":first_name, "last_name":last_name, "email":email, "password":password, "user_type":user_type,"profile_pic":profile_pic, csrfmiddlewaretoken:csrfval, 'contactno':contactno},
	success : function() {
		// $('#signupModal').fadeOut(5000); 
		$('.sucessmsg').html("<h4>Sign-up done sucessfully</h4>");
		$('.sucessmsg').removeClass('hidden');
			$('.sucessmsg').html("<div class=\"alert alert-success sucessmsg sucessmsg\" role=\"alert\"><h4>Sign-Up Successfull.Please Check Your Email</h4></div>")
			setTimeout('$("#signupModal").hide()',5000);
			$('.signupform').trigger('reset');
			$('.modal-backdrop').remove();
			
		}
	});
	
	return false;
});
// signup processing end
$('#signupModal').on('hidden.bs.modal', function (e) {
  $('.sucessmsg').html('');
})

//login
$("#login").click(function(){
  csrfval = $('input[name="csrfmiddlewaretoken"]').val();
	$(".user-login-error").text("");
	$(".pass-login-error").text("");
	login_username = $("#login_username").val();
	login_password = $("#login_password").val();
	console.log(csrfval)
    
    $.ajax({
      type: "POST",
      url:"/frontend/login/",
      data: {"login_username":login_username, "login_password":login_password, csrfmiddlewaretoken:csrfval}, 
      dataType:'json',
      success: function() {
        $("#myModal").modal('hide');
        location.reload();
      },
      error: function (error) {
        if (error.responseJSON.password) {
        	$(".pass-login-error").text(error.responseJSON.password[0]);
        }
      }
    });
});
//changepassword
// $("#changepassform").click(function(){
// 	csrfval = $('input[name="csrfmiddlewaretoken"]').val();
// 	$(".user-confirmpassword-error").text("");
// 	$(".user-newpassword-error").text("");
// 	$(".user-oldpassword-error").text("");
// 	$(".user-error").text("");
// 	current_password=$("#current_password").val();
// 	new_password=$("#new_password").val();
// 	// reset_password= $("#reset_password").val();
// 	user= $("#user").val();
// 	// console.log("user val"+ user);
// 	$.ajax({
// 		data: {"current_password": current_password,"new_password": new_password, "user": user, csrfmiddlewaretoken:csrfval},
// 			url:"/frontend/index",
// 			type: "POST",
// 			dataType:'json',
// 			success: function(data) {
// 				$(".reset-password-success-msg").text(data.msg);
// 				$(".reset-password-success-msg").show();
// 				setTimeout(function(){
// 			      $(".reset-password-success-msg").fadeOut(4000);        
// 			  	});
// 				$("#reset-password-form")[0].reset();
// 		},
// 		error: function (error) {
// 			$("#reset-password-form").find('.error').show();
// 			$(".reset-password-success-msg").hide();
// 			if (error.responseJSON.old_password) {
// 				$(".user-oldpassword-error").text(error.responseJSON.old_password[0]);
// 			}
// 			if (error.responseJSON.new_password){
// 				$(".user-newpassword-error").text(error.responseJSON.new_password[0]);	
// 			}
// 			if (error.responseJSON.reset_password){
// 				$(".user-confirmpassword-error").text(error.responseJSON.reset_password[0]);	
// 			}
// 			if (error.responseJSON.user){
// 				$(".user-error").text(error.responseJSON.user[0]);	
// 			}
// 		}
// 	});
// });

$('ul.nav li.dropdown').hover(function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
}, function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
});

// booknow ajax starts here
$("#book_now_btn").click(function() {
 	
 	$('.sucess_msg').addClass('hidden');
	csrfval = $('input[name="csrfmiddlewaretoken"]').val();
	var date=$("#date").val();
	var totalguest = $("#totalguest").val();
	var menu=$(".menu").val();
	var booking_name=$("#booking_name").val();
	var booking_email = $("#booking_email").val();
	var booking_no = $("#booking_no").val();
	var user = $("#user").val();
	var venue = $("#venue").val();
	message=$("#contact_message").val();
	x=$("#contact-form").serializeArray();
	$.ajax({
		url: "/frontend/venue-booking/",
		type: "POST",
		data: {"date":date, "totalguest":totalguest,"menu":menu, "booking_name":booking_name, "booking_email":booking_email, "booking_no":booking_no,csrfmiddlewaretoken:csrfval, "venue":venue, "user":user},
 		success : function() {
 			$('.sucess_msg').removeClass('hidden');
			$('.sucess_msg').html("<div class=\"alert alert-success sucess_msg sucess_msg\" role=\"alert\"><h4>Booking Sucessful</h4></div>")
			setTimeout('$("#bookingModal").hide()',3000);
			$('.bookingForm').trigger('reset');
			$('.modal-backdrop').remove();
		}  
  		});
	});
// booknow ajax ends here
$('#bookingModal').on('hidden.bs.modal', function (e) {
  $('.sucess_msg').html('');
})

$('#updatebookingModal').on('shown.bs.modal', function (e) {
    var this_ = $(e.relatedTarget)
	venue_id = this_.data()['id']
    a1 = this_.parents('.book_obj');
	email = a1.find('.email1').attr('value');
	user = a1.find('.user1').val();
	contact = a1.find('.contact1').attr('value');
	date = a1.find('.date1').attr('value');
	name = a1.find('.name1').attr('value');
	no_people = a1.find('.no_people1').attr('value');
	menu = a1.find('.menu1').attr('value');
	venue = a1.find('.location1').attr('value');
	if(menu == 'Veg'){
		$('#veg').attr('checked', 'checked')
	}
	else if(menu == 'Non-Veg'){
		$('#non-veg').attr('checked', 'checked')
	}
	$('.user2').attr('value', user);
	$('.venue_id').attr('value', venue_id);
	$('.venue2').attr('value', venue);
	$('.date2').attr('value', date);
	$('.name2').attr('value', name);
	$('.guest2').attr('value', no_people);
	$('.email2').attr('value', email);
	$('.contact2').attr('value', contact);
})

$('#updatebookingModal').on('hidden.bs.modal', function (e) {
  $('.sucess_msg1').html('');
})

$("#updatebutton").click(function() {
	csrfval = $('input[name="csrfmiddlewaretoken"]').val();
	var date = $("#date").val();
	var totalguest = $("#totalguest").val();
	var menu = $("input[name=menu]:checked").val();
	var booking_name = $("#booking_name").val();
	var booking_no = $("#booking_no").val();
	var booking_email = $("#booking_email").val(); 
	var venue_id = $(".venue_id").val();
	var venue = $(".venue2").val();
	var user = $(".user2").val();
	$.ajax({
		url: "/frontend/booking-event-update/"+venue_id+"/",
		type: "POST",
		data: {"date":date, "totalguest":totalguest, "user":user, "venue":venue, "menu":menu, "booking_name":booking_name, "booking_no":booking_no, "booking_email":booking_email, csrfmiddlewaretoken:csrfval},
		success : function() {
				$('.sucess_msg1').html("<div class=\"text-success sucess_msg1 sucess_msg1\"><h4>Booking Sucessful</h4></div>")
				// $('.sucess_msg1').html("<h4>Update done sucessfully</h4>")
				$('.sucess_msg1').removeClass('hidden');
				$('.updatebookingform').trigger('reset');
				// setTimeout('$("#updatebookingModal").hide()',1000);
			}
		});
		
		return false;
});

// Start forgotpwd function
$("#forgot_password_btn").click(function(){
	csrfval = $('input[name="csrfmiddlewaretoken"]').val();
	// $(".forgot_passwd_error").text("");
	email = $("#emailid").val();
	$.ajax({
		url: "/frontend/forgot_password",
		type: "POST",
		data: {"email":email, csrfmiddlewaretoken:csrfval},
		success: function(data) {
			alert("forgot password");
			// $("#forgotpassword-form").hide();
			// $("#forgotpassword-footer").hide();
			// $(".forgot_password_success").show();
			// $(".forgot_password_success").text(data.msg);
		},
		error: function (error) {
			// $(".forgot_passwd_error").text(error.responseJSON.email[0]);
		}
	});
});
// End forgotpwd function
//forgotpassword request
// $("#newpasswdbtn").click(function(){
// 	alert("yami");
// 	debugger
// 	csrfval = $('input[name="csrfmiddlewaretoken"]').val();
// 	// $(".forgot_passwd_error").text("");
// 	password = $("#password").val();
// 	confirm_password = $("#confirm_password").val();
// 	$.ajax({
// 		url: "/frontend/password_setting_for_forgotpassword",
// 		type: "POST",
// 		data: {"password":password,"confirm_password":confirm_password, csrfmiddlewaretoken:csrfval},
// 		success: function(data) {
// 			alert("forgot password");
// 			// $("#forgotpassword-form").hide();
// 			// $("#forgotpassword-footer").hide();
// 			// $(".forgot_password_success").show();
// 			// $(".forgot_password_success").text(data.msg);
// 		},
// 		error: function (error) {
// 			// $(".forgot_passwd_error").text(error.responseJSON.email[0]);
// 		}
// 	});
// });

//forgot password request end
// typeahead goes here

var station_name_list = new Bloodhound({
	datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
	queryTokenizer: Bloodhound.tokenizers.whitespace,
	limit: 10,
	prefetch: {
	cache:false, 
	url: '/frontend/search-venue-location-typeahead/',
	filter: function(list) {
		return $.map(list, function (station_name, station_id) {
			    return {
			    	id : station_id,
			    	name: station_name,
			    }
			});
		}
	}
}); 

station_name_list.initialize();

$('.typeahead').typeahead(null, {
	name: 'station_name_list',
	displayKey: 'name',    
	source: station_name_list.ttAdapter(),

	templates: {
		empty: [
			'<div class="empty-message">',
			'Unable to find any match for venue location',
			'</div>'].join('\n'), 
		suggestion: Handlebars.compile('<div data-case-id={{id}} class="row typeahead-row-styling">{{name}}<span class="case_type_sugg pull-right">{{type}}</span></div>')
	} 
}); 

$('.typeahead').on('typeahead:selected', function(evt, item) {
	station_id = item.id;
	$('#searchbar').attr("action", '/frontend/search-results-venue/'+station_id);
	});
// type ahead ends here
$('.search-btn1').click(function() {
	debugger
	if ($("#searchbar").attr("action") == "") {
		cat = $(".typeahead").attr('value');
		if (cat != null) {
			$('#searchbar').attr("action", '/frontend/search-results-venue/'+station_id);
			$("#searchbar").submit();
		} else {
			if (cattext == "") {
				alert('please insert some value');
			}
		}
	} else {
		$("#searchbar").submit();
	}
});
